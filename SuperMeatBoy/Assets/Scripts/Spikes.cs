﻿using UnityEngine;
using System.Collections;

public class Spikes : MonoBehaviour {
	//PRIVATE VARS
	private bool active, initialized;
	//PUBLIC VARS
	public float initTime;
	public float activeTime;
	private float temp;	
	//START=====================================================================================================================
	void Start () {
		active = true;
		initialized = false;
	
	}
	
	//UPDATE=====================================================================================================================
	void Update () {
		//init
		if (!initialized) {
			temp += Time.deltaTime;
			if (temp >= initTime) {
				active = false;
				transform.GetChild (0).gameObject.SetActive (false);
				temp = 0;
				initialized = true;
			}
		} 
		else {
			temp += Time.deltaTime;
			if (temp >= activeTime) {
				if (active) {
					transform.GetChild (0).gameObject.SetActive (false);
					active = false;

				} else {
					active = true;
					transform.GetChild (0).gameObject.SetActive (true);

				}
				temp = 0;
			}
		}
	}
}
