﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	//PRIVATE VARS
	private GameObject player;
	private float  width;
	private float height;
	private float length;
	private Vector2 input;
	private Animator anim;
	//PUBLIC VARS
	public float    speed = 10;
	public float    accel = 5f;
	public float airAccel = 3f;
	public float     jump = 12; 
	public GameObject spawnPoint;
	AudioSource aSource;
	public AudioClip clip_jump, clip_hit;

	//START======================================================================================================
	void Start () {
		//Pillamos el objeto player, su anchura y altura
		anim = GetComponent<Animator>();
		player = transform.gameObject;
		aSource = GetComponent<AudioSource> ();
		width = player.GetComponent<Collider2D>().bounds.extents.x + 0.1f;
		height = player.GetComponent<Collider2D>().bounds.extents.y + 0.2f;
		length = 0.05f;


	}
	
	//UPDATE=====================================================================================================================
	void Update () {
        //INPUTS
      
        if (Input.GetKey(KeyCode.LeftArrow))
			input.x = -1;
		else if(Input.GetKey(KeyCode.RightArrow))
			input.x = 1;
		else
			input.x = 0;

		if (Input.GetKeyDown (KeyCode.Space)) {
			aSource.clip = clip_jump;
			aSource.Play ();
			input.y = 1;
		}
			
		
		//Reverse player
		transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, (input.x == 0) ? transform.localEulerAngles.y : (input.x - 1) * 90, transform.localEulerAngles.z);

		//MOVE
		Move ();

		//ANIMATIONS
		if (Ground ()) {
			anim.SetBool ("jump", false);
            if (input.x != 0) {
				anim.SetBool ("run", true);
			} else {
				anim.SetBool ("run", false);
			}

		} else {
			anim.SetBool ("jump", true);
			anim.SetBool ("run", false);
        }

	}
	//PLAYER MOVEMENT
	public void Move()
    {

        //HORIZONTAL MOVEMENT
        GetComponent<Rigidbody2D>().AddForce(new Vector2(((input.x * speed) - GetComponent<Rigidbody2D>().velocity.x) * (Ground() ? accel : airAccel), 0));
		//JUMP
		GetComponent<Rigidbody2D>().velocity = new Vector2( (input.x == 0 && Ground()) ? 0 : GetComponent<Rigidbody2D>().velocity.x, 
														  (input.y == 1 && Touch()) ? jump : GetComponent<Rigidbody2D>().velocity.y);

		//Wall Jump
		if(Wall() && !Ground() && input.y == 1)
			GetComponent<Rigidbody2D>().velocity = new Vector2(-wallDirection() * speed * 0.75f, GetComponent<Rigidbody2D>().velocity.y);

		input.y = 0;

	}

	//PLAYER GROUNDED==============================================================================================================
	public bool Ground()
    {
        //Creamos 3 raycast en la parte inferior del player, 1 en cada esquina y 1 en el centro
        bool ray1 = Physics2D.Raycast(new Vector2(player.transform.position.x, player.transform.position.y - height), -Vector2.up, length);
		bool ray2 = Physics2D.Raycast(new Vector2(player.transform.position.x + (width - 0.2f), player.transform.position.y - height), -Vector2.up, length);
		bool ray3 = Physics2D.Raycast(new Vector2(player.transform.position.x - (width - 0.2f), player.transform.position.y - height), -Vector2.up, length);

		//si cualquiera de estos esta chocando con algo es que estamos en el suelo
		if(ray1 || ray2 || ray3)
			return true;
		// si no estamos en el aire o en una pared
		else
			return false;
	}

	//WALL COLLISION================================================================================================================
	public bool Wall()
	{
		//Creamos 2 Raycast, 1 en cada lado del player para comprobar la colision con las paredes
		bool left = Physics2D.Raycast(new Vector2(player.transform.position.x - width, player.transform.position.y), -Vector2.right, length);
		bool right = Physics2D.Raycast(new Vector2(player.transform.position.x + width, player.transform.position.y), Vector2.right, length);

		//Si cualquiera de estos colisiona con algo es que estamos en una pared
		if(left || right)
			return true;
		else
			return false;
	}




	//PLAYER EN EL AIRE=============================================================================================================
	public bool Touch()
	{
		//Si estamos en el suelo o en una pared es que estamos tocando algo
		if(Ground() || Wall())
			return true;
		else
			return false;
	}

	//WALL COLLISION DIRECTION=====================================================================================================
	public int wallDirection()
	{
		//Creamos un Raycast hacia cada lado
		bool left = Physics2D.Raycast(new Vector2(player.transform.position.x - width, player.transform.position.y), -Vector2.right, length);
		bool right = Physics2D.Raycast(new Vector2(player.transform.position.x + width, player.transform.position.y), Vector2.right, length);

		//dependiendo de cual colisiones rendremos una pared en la derecha o en la izquierda
		if(left)
			return -1;
		else if(right)
			return 1;
		else
			return 0;
	}
	//DIE==========================================================================================================================
	public void Die(){
		transform.position = spawnPoint.transform.position;
		aSource.clip = clip_hit;
		aSource.Play ();
	}
	//OnCollisionEnter=============================================================================================================
	void OnCollisionEnter2D(Collision2D col){
        if (col.gameObject.tag == "Dead")
        {
            Die();
        }
		if (col.gameObject.tag == "End") {
			Application.LoadLevel ("Menu");
		}
    }
	//OnTriggerEnter===============================================================================================================
	void OnTriggerEnter2D(Collider2D col){
		if (col.gameObject.tag == "TriggerTrap")
        {
            col.GetComponentInParent<Smasher> ().Active();
		}

	}
}