﻿using UnityEngine;
using System.Collections;

public class Smasher : MonoBehaviour {
	//PRIVATE VARS
	bool active = false;
	bool blocked = false;
	float timer = 0;
	Vector2 initPos;
	//PUBLIC VARS
	public float recoverSpeed;
	public float smashSpeed;
	//START======================================================================================================
	void Start () {
		initPos = transform.position;
	}
	
	//UPDATE======================================================================================================
	void Update () {
		//CheckImpact ();
		if (blocked) {
			timer += Time.deltaTime;
			//Debug.Log ("COUNTDOWN :" + timer);
			if (timer > 1.5) {
				Debug.Log ("GO BACK");
				timer = 0;
				blocked = false;
				active = false;
			}
		}
		if (active && !blocked) {
			transform.Translate (new Vector2(0,-smashSpeed*Time.deltaTime));
			//Debug.Log ("BAJANDO");
		}
		else if(!blocked){
			if (transform.position.y < initPos.y) {
				transform.Translate (new Vector2(0,recoverSpeed*Time.deltaTime));
				//Debug.Log ("SUBIENDO");
			}
		}
	
	}
	//ACTIVE======================================================================================================
	public void Active(){
		if (!active && transform.position.y >initPos.y -0.2f) {
			active = true;
		}


	}
	/*void CheckImpact(){
		RaycastHit2D ray1 = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y -GetComponent<Collider2D>().bounds.extents.y + 0.2f), -Vector2.up,0.1f);
		if(ray1.collider.gameObject.tag =="Floor"){
			Debug.Log ("Ray in the floooooor");
			blocked = true;
		}
	}*/
	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Floor") {
			//Debug.Log ("BLOCKING");
			blocked = true;

		}
	}
}
