﻿using UnityEngine;
using System.Collections;

public class Scr_Moving_Platform : MonoBehaviour
{

    private float pos_x = 0; //float de posicion por defecto
    public float max_x = 5; //float de maxima posicion a la que se desplazar sumada a pos_x
    public float speed = 2; //float de movimiento del rigidbody velocity
    private Rigidbody2D rb2d;
    private FixedJoint2D joint;
    public bool direction = false; //false = left, true = right
    private Rigidbody2D player_rb2d;

    void Start()
    {
        pos_x = gameObject.transform.position.x; //cojemos la posicion original en el Start
        rb2d = gameObject.GetComponent<Rigidbody2D>();
        joint = gameObject.GetComponent<FixedJoint2D>();
    }

    void Update()
    {
        moving();
    }


    void moving()
    {
        if (gameObject.transform.position.x < (pos_x - max_x))
        {
            direction = true;
        }
        else if (gameObject.transform.position.x > (pos_x + max_x))
        {
            direction = false;
        }
        if (direction)
        {
            rb2d.velocity = new Vector2(speed, rb2d.velocity.y);
        }
        else
        {
            rb2d.velocity = new Vector2(-speed, rb2d.velocity.y);
        }
        return;
    }

    void OnCollisionStay2D(Collision2D col)
    {
        if (((!Input.GetButton("Jump")) || (Input.GetAxis("Horizontal") == 0)) &&
            col.gameObject.tag == "Player")
        {
            player_rb2d = col.gameObject.GetComponent<Rigidbody2D>();
            player_rb2d.velocity = new Vector2(rb2d.velocity.x, player_rb2d.velocity.y);
        }
    }
}