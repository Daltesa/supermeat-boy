﻿using UnityEngine;
using System.Collections;

public class Sierra : MonoBehaviour {
	//PRIVATE VARS
	private float minD, maxD;
	private int dir;
	//PUBLIC VARS
	public bool moving;
	public bool verticalMoving;
	public float distance;
	public int speed;

	//START=================================================================================================================================
	void Start () {
		dir = 1;
		if (moving) {
			if (verticalMoving) {
				minD = transform.position.y - distance;
				maxD = transform.position.y + distance;
			} 
			else {
				minD = transform.position.x - distance;
				maxD = transform.position.x + distance;
			}
		
		}

	}
	
	//UPDATE=================================================================================================================================
	void Update () {
        //transform.Rotate(Vector3.forward *5 *Time.deltaTime);
        if (moving) {
			if (verticalMoving) {
				transform.Translate (new Vector2 (0,speed * dir * Time.deltaTime));
				if (transform.position.y > maxD || transform.position.y < minD) {
					dir = dir * -1;
				}
			} 
			else {
				transform.Translate (new Vector2 (speed * dir * Time.deltaTime,0));
				if (transform.position.x > maxD || transform.position.x < minD) {
					dir = dir * -1;
				}
			
			}
			
		}
	
	}
}
